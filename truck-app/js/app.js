
var ng = angular.module('truck-mentorize', [ "ngRoute", 'ngMaterial', 'ngMessages','ngAnimate','ngAria' ]);
var version = '?v1.0.0';
var hostUrl = 'http://18.221.50.53:8000/';
ng.config([ '$routeProvider', '$controllerProvider', '$provide', function($routeProvider, $controllerProvider, $provide) {
    // Lazy loading
    ng.lazy = {
        controller: $controllerProvider.register,
        //directive: $compileProvider.directive,
        //filter: $filterProvider.register,
        //factory: $provide.factory,
        service: $provide.service
    };

    $routeProvider
        .when('/login/', {
            templateUrl: 'src/login/form.html'+version
        }).when('/logout', {
            templateUrl: 'src/login/logout.html'+version
        })
        .when('/admin/company/:company_id/', {
            templateUrl: 'src/super-admin/company/form.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/super-admin/company/companyController.js'+version);
                }]
            }
        }).when('/admin/companies/', {
            templateUrl: 'src/super-admin/company/list.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/super-admin/company/companyController.js'+version);
                }]
            }
        }).when('/admin/gpslist/', {
            templateUrl: 'src/super-admin/gps/gps_list.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/super-admin/gps/gpsController.js'+version);
                }]
            }
        }).when('/admin/gpsform/', {
            templateUrl: 'src/super-admin/gps/gps_form.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/super-admin/gps/gpsController.js'+version);
                }]
            }
        }).when('/company/add/drivers/', {
            templateUrl: 'src/company/driver/add_drivers.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/driver/driverController.js'+version);
                }]
            }
        }) .when('/company/driverlist/', {
            templateUrl: 'src/company/driver/driver_list.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/driver/driverController.js'+version);
                }]
            }
        }).when('/company/add/trucks/', {
            templateUrl: 'src/company/trucks/add_trucks.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/trucks/truckController.js'+version);
                }]
            }
        }) .when('/company/trucklist/', {
            templateUrl: 'src/company/trucks/truck_list.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/trucks/truckController.js'+version);
                }]
            }
        }).when('/company/add/trailers/', {
            templateUrl: 'src/company/trailer/add_trailers.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/trailer/trailerController.js'+version);
                }]
            }
        }).when('/company/trailerlist/', {
            templateUrl: 'src/company/trailer/trailers_list.html'+version,
            resolve: {
                ctrl: ['lazyService', function (lazyService) {
                    return lazyService.loadScript('src/company/trailer/trailerController.js'+version);
                }]
            }
        }) .otherwise({
            redirectTo: '/login/'
        });
}]).config(function($locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
});


ng.factory('lazyService', [ '$http', '$rootScope', function($http, $rootScope) {
    var promisesCache = {};

    return {
        loadScript: function(name) {
            
            var path = name;
            var promise = promisesCache[name];
            if (!promise) {



                //console.log('in loading', $rootScope.loading_bar);
                //$rootScope.loading_bar = 'indeterminate';
                //setTimeout(function () {
                promise = $http.get(path,{ cache: true});
                promisesCache[name] = promise;
            
                return promise.then(function(result) {
                    eval(result.data);
                    //$rootScope.loading_bar = 0;
                    //console.log('out loading', $rootScope.loading_bar);
                });
                //}, 2000);
            }
            return promise;
        }
    }
}]);


var loader_lock = [];
function ajaxService($http, $q, $window, ErrorHandler,MessageHandler, loading) {
    this.http = function (request) {
        time = (new Date()).getTime();
        if(!loader_lock.length)
            loading.show();
           
        request['headers']['access-token'] = $window.localStorage.token;
        //request['headers']['app-client'] = "cr-erp";
        request['timeout'] = 6000;
        request.url = hostUrl + request.url;
        if(request.method.toLowerCase() == 'get') {
            punchh = 'time=' + time;
            request.url += (request.url.indexOf('?') > -1 ? '&'+punchh : '?'+punchh);
        }
        var deferred = $q.defer();
        //console.log(request);
        $http(request).success(function (response, status, headers, config) {
            var otime = loader_lock.pop();
        console.log('aa gaya', otime, status, headers, config);
          
            setTimeout(function(){
                if(!loader_lock.length){
                    loading.hide();
                }
            }, 200);
            deferred.resolve(response);
        }).error(function (error, status, headers, config) {
            loader_lock.pop();
            //console.log('success', loader_lock);
            if(!loader_lock.length)
                loading.hide();
            if(request['responseType'] == 'arraybuffer'){
                if ('TextDecoder' in window) {
                    // Decode as UTF-8
                    var dataView = new DataView(error);
                    var decoder = new TextDecoder('utf8');
                    error = JSON.parse(decoder.decode(dataView));
                } else {
                    // Fallback decode as ASCII
                    var decodedString = String.fromCharCode.apply(null, new Uint8Array(error));
                    error = JSON.parse(decodedString);
                }
            }
            ErrorHandler.handle(error, status);
            //console.log('error:',error, 'status:',status);
            if(status != -1)
                error['status'] = status;
            deferred.reject(error);
        });
        return deferred.promise;
    }
}

function ErrorHandler($location, MessageHandler) {
    this.handle = function (error, status) {
        if(status == -1){
            this.showError("Request timeout occurred, please try again !");
        }else if(status == '401'){
            //authService.clearInfo();
            $location.path("/login");
            this.showError("Your session has expired, please login again !");
        }else if(error && error !== undefined && error['responsetext'] !== undefined && error['responsetext'] !== ''){
            this.showError(error['responsetext']);
        }else{
            this.showError('Server is not responding, please try again !');
        }
    };
    this.showError = function (text) {
        MessageHandler.showErrorMsg(text);
    }
}

function MessageHandler($mdToast) {
    this.showSuccessMsg = function (text) {
        $mdToast.show(
            $mdToast.simple()
                .toastClass('md-toast-done')
                .textContent('succesful')
                .position('bottom right')
                .hideDelay(3000)
        );
    };
    this.showErrorMsg = function (text) {
        $mdToast.show(
            $mdToast.simple()
                .toastClass('md-toast-error')
                .textContent(text)
                .position('bottom right')
                .hideDelay(3000)
        );
    };
    this.showInfoMsg = function (text) {
        $mdToast.show(
            $mdToast.simple()
                .toastClass('md-toast-info')
                .textContent(text)
                .position('bottom right')
                .hideDelay(3000)
        );
    };
}


ng.service('MessageHandler', ['$mdToast', MessageHandler])
.service('ErrorHandler', ['$location', 'MessageHandler', ErrorHandler])
.service('ajaxService', ['$http', '$q', '$window', 'ErrorHandler','MessageHandler', 'loader' ,ajaxService]);


ng.config(function($mdThemingProvider) {
  //$mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
  //$mdThemingProvider.theme('dark-orange').backgroundPalette('purple').dark();
  $mdThemingProvider.theme('default')
    .primaryPalette('purple', {
        'default': '900',
        'hue-1': '800',
        'hue-2': '700',
        'hue-3': 'A700'
    }).accentPalette('pink', {
        'default': 'A400',
        'hue-1': '500',
        'hue-2': '300',
        'hue-3': 'A100'
    }).warnPalette('deep-orange', {
        'default': 'A700',
        'hue-1': 'A400',
        'hue-2': '500',
        'hue-3': '300'
    });
   /*$mdThemingProvider.definePalette('truckPalette', {
        '50': '25274d',
        '100': '464866',
        '200': '2964ba',
        '300': '2e9cca',
        '400': 'aaabb8',
        '500': 'e53935',
        '600': 'E65100',
        '700': 'FB8C00',
        '800': 'FBC02D',
        '900': 'FFE082',
        'A100': '006064',
        'A200': '009688',
        'A400': '00E5FF',
        'A700': 'E0F7FA',
        //'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
                                            // on this palette should be dark or light

        //'contrastDarkColors': ['50', '100', '200', '300', '400'], //hues which contrast should be 'dark' by default
        //'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });
  $mdThemingProvider.theme('default')
    .primaryPalette('truckPalette', {
      'default': '50', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '200', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': '300' // use shade A100 for the <code>md-hue-3</code> class
    })
    // If you specify less than all of the keys, it will inherit from the
    // default shades
    .warnPalette('truckPalette', {
      'default': '500', // use shade 200 for default, and keep all other shades the same
      'hue-1': '600',
      'hue-2': '700',
      'hue-3': '800'
    })
    .accentPalette('truckPalette', {
      'default': 'A100', // use shade 200 for default, and keep all other shades the same
      'hue-1': 'A200',
      'hue-2': 'A400',
      'hue-3': 'A700'
    });*/
}).run(function($rootScope) {
    $rootScope.loading_bar = 0;
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
        $rootScope.loading_bar = 'indeterminate';
        console.log('in loading', $rootScope.loading_bar);
    });
    $rootScope.$on( "$routeChangeSuccess", function(event, next, current) {
        setTimeout(function () {
            $rootScope.loading_bar = '';
            console.log('out loading', $rootScope.loading_bar);
        }, 200);

    });
    //console.log('in run', $rootScope.loading_bar);
}).service('loader', ['$mdDialog', function($mdDialog){
     
     return {
       hide: function hide(){
          setTimeout(function(){
            $mdDialog.cancel();
                   //$rootScope.$emit("hide_wait"); 
                },5);
      },
       show: function show(){
              $mdDialog.show({
                template: '<md-dialog id="plz_wait" style="background-color:transparent;box-shadow:none;overflow:hidden;">' +
                            '<div layout="row" layout-align="center center" aria-label="wait">' +
                                '<md-progress-circular md-mode="indeterminate" md-diameter="70"></md-progress-circular>' +
                            '</div>' +
                         '</md-dialog>',
                parent: angular.element(document.body),
                clickOutsideToClose:false,
                fullscreen: false
              })
              .then(function(answer) {
                
              });
       }
     }
}]).directive('navigation', ['$mdSidenav', function ($mdSidenav) {
        console.log('abe chal');
        return {
            restrict: 'A',
            controller:function($scope){
              $scope.toggleLeft = buildToggler('left');

                function buildToggler(componentId) {
                    //console.log('buildToggler', componentId);
                    return function() {
                        $mdSidenav(componentId).toggle();
                        //console.log('$mdSidenav', componentId);
                    };
                };
            }
        };
    }]).directive('isCompany', ['authenticationService', function(authenticationService) {
          return {
            restrict: 'A',
            controller:function($scope){
                $scope.name=window.localStorage.name;
               /* $scope.is_superuser = authenticationService.isSuperuser()*/;
              

            }
        };
    }]).directive('isSuperuser', ['authenticationService', function(authenticationService) {
          return {
            restrict: 'A',
            controller:function($scope){
                $scope.name=window.localStorage.name;
               /* $scope.is_superuser = authenticationService.isSuperuser()*/;
               $scope.is_superuser=authenticationService.isSuperuser();

            }
        };
    }]).directive('passwordConfirm', ['$parse', function ($parse) {
 return {
    restrict: 'A',
    scope: {
      matchTarget: '=',
    },
    require: 'ngModel',
    link: function link(scope, elem, attrs, ctrl) {
      var validator = function (value) {
        ctrl.$setValidity('match', value === scope.matchTarget);
        return value;
      }
 
      ctrl.$parsers.unshift(validator);
      ctrl.$formatters.push(validator);
      
      // This is to force validator when the original password gets changed
      scope.$watch('matchTarget', function(newval, oldval) {
        validator(ctrl.$viewValue);
      });

    }
  };
}]);

