/**
 * Created by ashutosh on 10/18/16.
 */
var ng = angular.module('truck-mentorize');

function authenticationService($window) {



    var auth = {
        isLoggedIn: false,
        userInfo:{
            name: '',
            email: '',
            role: '',
            status: '',
            isAuthenticated  : false
        },
        getInfo: function () {
            var userInfo = {};
            userInfo['name'] =  $window.localStorage.name;
            userInfo['email'] = $window.localStorage.email;
            userInfo['role'] = $window.localStorage.role;
            userInfo['status']  = $window.localStorage.uStatus;
            userInfo['phone']  = $window.localStorage.phone;
            userInfo['repMgr']  = $window.localStorage.repMgr;
            userInfo['token']  = $window.localStorage.token;
            userInfo['old_user_id']=$window.localStorage.old_user_id;
            userInfo['company_id']=$window.localStorage.company_id;
            userInfo['user_id']=$window.localStorage.user_id;
            userInfo['isAuthenticated'] = $window.localStorage.isAuthenticated;
       
            return userInfo;
        },
        setInfo: function (user) {
            console.log(user);

            $window.localStorage.token = user['access_token'];
            $window.localStorage.name = user['name'];
            //$window.localStorage.email = user['email'];
            $window.localStorage.role = user['role_permission']['roles'];
            //$window.localStorage.uStatus = user['user_status'];
            $window.localStorage.phone = user['phone'];
            $window.localStorage.is_super_user = user['is_super_user'];
             console.log(user);
            $window.localStorage.user_id=user['user_id'];
            $window.localStorage.old_user_id=user['user_context']['old_user_id'];
            $window.localStorage.company_id=user['company_id'];
                 console.log($window.localStorage.old_user_id);  
                 
         
           $window.localStorage.permissions = user['role_permission']['permissions'];
            $window.localStorage.isAuthenticated = true;
        },
        clearInfo: function () {
            this.isLoggedIn = false;
            delete $window.localStorage.company_id;
            delete $window.localStorage.user_id;
            delete $window.localStorage.is_super_user;
            delete $window.localStorage.phone;
            //Remove window session storage code
            delete $window.localStorage.token;
            delete $window.localStorage.name;
            delete $window.localStorage.ename;
            delete $window.localStorage.email;
            delete $window.localStorage.role;
            delete $window.localStorage.isAuthenticated;
        },
        getAuthToken: function () {
            return $window.localStorage.token;
        },
     
        isOlduser: function() {
              
            return $window.localStorage.old_user_id ;
        },
        isSuperuser: function() {
            return $window.localStorage.is_super_user ==="1";
        },
        getRole: function () {
            return $window.localStorage.role;
        },
        getPermissions: function () {
            $window.localStorage.permissions = ['dew', 'dew1'];
            return $window.localStorage.permissions;
        },
        isPermitted: function (perms) {
            //return true;
            var isPermitted = false;
            if($window.localStorage.permissions !== undefined) {
                angular.forEach(perms, function (perm) {
                    if ($window.localStorage.permissions.indexOf(perm.trim()) >= 0)
                        isPermitted = true;
                });
            }
            return isPermitted;
        },
        isNewUser: function () {
            return ($window.localStorage.uStatus=='P' || $window.localStorage.uStatus=='N' || !$window.localStorage.uStatus || $window.localStorage.uStatus == 'undefined');
        },
        updatePhone: function (phone) {
            $window.localStorage.phone = phone;
        },
        isAuthorized: function () {
            return ($window.localStorage.isAuthenticated == "true" && $window.localStorage.token && $window.localStorage.token != 'undefined');
        },
        setAuthorized: function (flag) {
            $window.localStorage.isLoggedIn = flag;
            $window.localStorage.isAuthenticated = flag;
        }
    };
    return auth;
}

ng.service('authenticationService', ["$window", authenticationService]);

function AuthController($scope,$rootScope, $location, $window, $interval, authenticationService, ajaxService) {

$rootScope.superuser_Ascompany= parseInt($window.localStorage.old_user_id) > 0? $window.localStorage.old_user_id : 0;



    $scope.BackAsSuperUser= function(id){

        var logoutsuperuser={
            old_user_id:$window.localStorage.old_user_id
        }


        console.log("ravi kalia");
        var request={headers:{},method:'POST',url:'accounts/superuser/assup/Oauth/',data:logoutsuperuser}
        ajaxService.http(request).then(function (response) {


          authenticationService.setInfo(response.data);
          $rootScope.is_superuser=authenticationService.isSuperuser();
          $rootScope.superuser_Ascompany=0;
          $location.path('/admin/companies/');


                        // else route to company dashboard
                        //$location.path('/company/');
                    }, function (error) {

                    });






    };

    $scope.initSignIn = function() {
        if(authenticationService.isAuthorized()){
            $location.path('/');
        }
        else {
            $scope.phone = authenticationService.getPhone();
            $scope.name = authenticationService.getName();
        }
    };

    $scope.logout = function() {
    console.log("logout functionaliy");
        authenticationService.clearInfo();

            $location.path('/login/');

   /*     $scope.phone = authenticationService.getPhone();
        authenticationService.clearAll();
        $scope.phone = authenticationService.setPhone($scope.phone);
        $location.path('/login');
        jQuery('body').addClass("login-wrapp");*/
    };

    $scope.initLogin = function() {
        if(authenticationService.isAuthorized()) {
                $location.path('/');
        }else{
            $scope.phone = parseInt(authenticationService.getPhone());
        }
    };

    
   
    $scope.signIn = function() {
            console.log($scope.formData);
             var request = {headers :{}, method: 'POST', url: 'accounts/superuser/Oauth/',data:$scope.formData};
                    ajaxService.http(request).then(function (response) {
                        //console.log(response.data);
                        //console.log(response.data);

                        authenticationService.setInfo(response.data);
                        $rootScope.is_superuser=authenticationService.isSuperuser();
          
               if($rootScope.is_superuser){
                        //if superadmin then route to:
                        $location.path('/admin/companies/');
                        
}
else{
                        $location.path('/company/driverlist/');
                    }
                        // else route to company dashboard
                        //$location.path('/company/');
                    }, function (error) {

                    });
                };
        }

var app = angular.module('truck-mentorize');

app.service('authenticationService', ["$window", authenticationService]);

app.controller('AuthController', ['$scope','$rootScope', '$location', '$window', '$interval', 'authenticationService', 'ajaxService', AuthController])
app.run(['$rootScope', '$location', '$window', 'ErrorHandler','authenticationService', function($rootScope, $location, $window, ErrorHandler, authenticationService) {
    $rootScope.$on("$routeChangeStart", function (event, nextRoute, currentRoute) {
        var path = '';
        if(nextRoute.$$route && nextRoute.$$route != 'undefined')
            path = nextRoute.$$route.originalPath;
        if(path){
            //console.log('path:' + path + ' :: ' + (path != '/user/form') + ' | role:' + (!authenticationService.getRole()) + ' | und:' + (authenticationService.getRole() == 'undefined') + ' | code:' + ($window.location.search.split('code=').length));
            if (!authenticationService.isAuthorized()){
                //$window.location.href = "/";
                //$window.location.href = "/#/";
                //ErrorHandler.handle({}, 401);
            }
        }
    });
}]);