function gpsController($scope, $routeParams, $mdDialog, ajaxService,MessageHandler){
  $scope.company_id = parseInt($routeParams.company_id) > 0 ? parseInt($routeParams.company_id) : 0;
  $scope.title = "Add New Company";

/*********************gps add ******************/

  $scope.submitGpsForm=function(){

 var is_ok = true;

        if(is_ok && ($scope.gps['device_id'] === undefined || String($scope.gps['device_id']).length <2)){
            is_ok = false;
            MessageHandler.showErrorMsg("Device Id  is blank !");
            return true
        }
    else if( is_ok && ($scope.gps['ecm_gps_number'] === undefined || $scope.gps['ecm_gps_number'].length<2)){
            is_ok = false;
            MessageHandler.showErrorMsg(" Ecm Gps Number is blank !");
            return true
        } 

        else if(is_ok && ($scope.gps['imei_number'] === undefined || $scope.gps['imei_number'].length<2)){
            is_ok = false;
            MessageHandler.showErrorMsg(" Imei Number is blank !");
            return true
        }
        else{

    var request = { headers:{}, method: 'POST', url: 'accounts/superuser/gpsdevices/',data:$scope.gps};
    ajaxService.http(request).then(function (response) {

      MessageHandler.showSuccessMsg(response['responsetext']);

    },  function (error) {

      console.log('error' + error);
    });

  };
}

/*************** gpslist*****************/


  $scope.getGpsList= function()
  {
    console.log("hehe");
    var request = {headers:{}, method: 'GET', url: 'accounts/superuser/gpsdevices/'};
    ajaxService.http(request).then(function (response) {
      $scope.gps_list = response['data'];



    },  function (error) {
      console.log('error' + error);
    });

  };


$scope.gps={

      "device_id":"",
      "ecm_gps_number":"",
      "imei_number":"" ,

    }

};


angular.module('truck-mentorize').lazy
.controller('gpsController', ['$scope', '$routeParams', '$mdDialog', 'ajaxService','MessageHandler', gpsController]);












